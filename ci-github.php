<?php

$CI_SECRET = 'xxxxx';
$BASE_DIR = '/var/www/html/';


$_SERVER['REQUEST_METHOD'] === 'POST' OR die(header('HTTP/1.1 405').'#METHOD_NOT_ALLOWED');

$data = file_get_contents('php://input');
$sign = substr($_SERVER['HTTP_X_HUB_SIGNATURE_256'] ??null, 7) OR die(header('HTTP/1.1 400').'#SIGNATURE');

$host_sign = hash_hmac('sha256', $data, $CI_SECRET);
hash_equals($host_sign, $sign) OR die(header('HTTP/1.1 401 #INVALID_SIGNATURE'));
$data = json_decode($data);

header('Content-Type: text/plain');
$branch = trim(explode('/', $data->ref)[2]);
$project = preg_replace('/\'"|\.\./', '', $branch ?: $data->repository->name);


chdir($BASE_DIR.$project);
$host_branch = trim(explode('/', file_get_contents("$BASE_DIR$project/.git/HEAD"))[2]);
$remote = parse_ini_file("$BASE_DIR$project/.git/config", $section=true)["branch $host_branch"]["remote"];


if ($host_branch !== $branch) {
    echo 'Branch not matched, Skip!';
} else {
    foreach ([
        "git fetch $remote $branch",
        "git merge $remote/$branch --no-edit",
        "composer --no-interaction --prefer-dist -o i",
        "php yii migrate --interactive=0",
    ] as $cmd) {
        $result = null;
        print "\n$cmd\n";
        passthru($cmd, $result);
        $result and print "Exit code: $result\n";
    }
}


echo "\n\ngit status\n";
system("git status");
echo "git log\n";
exec("git log -n 5", $output);

$commit = null;
foreach ((array)$output as $line) {
  if (trim($line)) {
    if (strpos($line, 'commit ') === 0) {
      $commit and print_r($commit);
      $commit = [$line];
    }
    else $commit[] = $line;
  }
}
$commit and print_r($commit);
