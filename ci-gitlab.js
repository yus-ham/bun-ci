import { appendFileSync } from 'fs'
import { basename } from 'path'


const VALIDATE = true
const TEST_PROJECT = 'press'

const BASE_DIR = '/var/www/html/'
const TOKEN = '!Xxx123'
const REMOTE = 'origin'


const projects = {
    'press': {
        branch: 'main',
        logFile: `/var/www/html/press-git/frontpage/ci/status.html`,
        directory: 'press-git',

        env: {
            //BUILD_OUTDIR: 'dist/portal',
            API_URL: '/press-dev/app-api/web',
            BASE_URL: '/press-dev/adminpanel',
        },
        extra_commands: [
            `bun install`,
            (p) => {
                const out = `[${date()}] Build started ...\n\n`
                appendFileSync(p.logFileTxt, out)
                appendFileSync(p.logFile, `${nl2br(out)}\n`)
            },
            { cmd: `bun vite build -l error`, isSuccess: o => !o.match(/Found \d+ errors? in/) },
            (p) => {
                const out = `[${date()}] CI finished with status: Success\n`
                appendFileSync(p.logFileTxt, out)
                appendFileSync(p.logFile, out)
                console.info(out)
            },
        ]
    },
    /*'portal-be': {
        env: {},
        branch: 'develop',
        pm2_proc_name: 'portal-be.3001',
        logFile: `/var/www/html/portal/ci-status-be.html`,
        extra_commands: [
            (p, out) => {
                spawnProcess(`pm2 restart 12 --update-env`, p)
                const proc = spawnProcess(`pm2 jlist`, p)

                for (const pm2_proc of JSON.parse(proc.stdout.toString())) {
                    if (pm2_proc.name === p.pm2_proc_name && pm2_proc.pid) {
                        out = `[${date()}] CI finished with status: Success\n`
                        appendFileSync(p.logFileTxt, out)
                        appendFileSync(p.logFile, out)
                        console.info(out)
                        return
                    }
                }

                out = `[${date()}] CI finished with status: Failed\n`
                appendFileSync(p.logFileTxt, out)
                appendFileSync(p.logFile, out)
                console.error(out)
            },
        ]
    },*/
}


// start ci-gitlab with pm2 id for portal-be
// pm2 start <ci-gitlab> -f  --interpreter-args -- pm2-id <portal-be>=<0>
let isPm2Id
for (let item of (process.argv || [])) {
        if (isPm2Id) {
                item = item.split('=').map(x => x.trim())

                if (item[0] && projects[item[0]]) {
                        projects[item[0]].pm2_id = item[1]
                }

                isPm2Id = false
                continue
        }

        if (item.startsWith('pm2-id')) {
                isPm2Id = true
        }
}

const server = Bun.serve({

    port: 9000,

    async fetch(req, p) {

        //console.info({req})

        try {
            p = await getProject(req)
            Bun.sleep(1).then(_ => startCI(p))
        } catch (e) {
            console.error(e)
            return new Response(e.message, e)
        }

        return new Response('OK\n\nCheck CI logs at: https://localhost/ci/' + basename(p.logFile))
    }
})

console.info(`Webhook server started on http://${server.hostname}:${server.port}`)


async function getProject(req) {
    const config = new URLSearchParams(req.headers.get('x-gitlab-token'))

    if (VALIDATE && !config.get('token'))
        throw { message: 'No Token', status: 401 }

    if (VALIDATE && config.get('token') !== TOKEN)
        throw { message: 'Invalid Token', status: 422 }

    const params = VALIDATE ? await req.json() : {}
    const push_branch = params.ref?.split('/')[2]
    const project_name = params.project?.path_with_namespace.split('/')[1] || TEST_PROJECT
    const project = projects[project_name] || {}

    if (VALIDATE && project.is_running)
        throw {message: 'Already running', status: 419 }

    if (VALIDATE && push_branch !== project.branch)
        throw { message: 'Invalid branch', status: 422 }

    project.name = project_name
    project.is_running = true
    project.logFileTxt = project.logFile.replace(/\.html$/, '.txt')

    return project
}

async function startCI(project, log) {
    await Bun.write(project.logFileTxt, (log = `[${date()}] CI started ...`) + `\n\n`)
    await Bun.write(project.logFile, `<!doctype html><body style="font-family:monospace">\n${log}<br><br>\n`)

    const opts = {
        stdout: 'pipe',
        stderr: 'pipe',
        env: { ...Bun.env, ...project.env },
        cwd: BASE_DIR + (project.directory || project.name)
    }

    //console.info({opts})

    new Array(
            (p) => console.info(`Project: ${p.name}`),
            `git status`,
            `git fetch ${REMOTE} ${project.branch}`,
            `git merge ${REMOTE}/${project.branch} --no-edit`,
            `git log -5`,
            ...project.extra_commands,
            (p) => {
                console.info(`---- END ------------------------------`)
                p.is_running = false
            },
        )
        .reduceRight((_onExit, cmd) => {
            const onExit = () => console.info(`---------------------------------------`) || _onExit()
            return async () => {
                if (typeof cmd === 'function')
                    return Promise.resolve().then(_ => cmd(project)).then(onExit)

                if (!cmd.cmd) {
                    cmd = { cmd }
                }

                cmd.isSuccess = () => true

                console.info(`CWD: ${opts.cwd}\nCMD: ${cmd.cmd}`)
                const proc = spawnProcess(cmd.cmd, opts)
                const err = proc.stderr.toString()

                if (err) {
                    const _err = `[${date()}] ${err}`
                    appendFileSync(project.logFileTxt, _err + '\n\n')
                    appendFileSync(project.logFile, `<font color="#dc3545">${nl2br(_err)}</font><br><br>\n`)
                }

                let out = proc.stdout.toString()

                if (proc.success && cmd.isSuccess(out)) {
                    out = `[${date()}] ${cmd.cmd}\n${out}\n\n`
                    appendFileSync(project.logFileTxt, out)
                    appendFileSync(project.logFile, nl2br(out) + '\n')
                    return onExit()
                }

                console.error(out)
                appendFileSync(project.logFileTxt, out + '\n\n\n')
                appendFileSync(project.logFile, `<font color="#dc3545">${nl2br(out)}</font><br><br>\n`)

                out = `[${date()}] CI finished with status: Failed`
                console.error(out)
                appendFileSync(project.logFileTxt, out + '\n')
                appendFileSync(project.logFile, `<font color="#dc3545">${out}</font>\n`)

                project.is_running = false
            }
        })()

}


const nl2br = (o) => o.replace(/\n/g, '<br>\n')
const date = (d) => { d = new Date(); d.setHours(d.getHours() - 1); return d.toString().slice(0, 29) + '0700' }
const spawnProcess = (cmd, opts) => { opts.cwd || (opts.cwd = BASE_DIR + (opts.directory || opts.name)); return Bun.spawnSync(cmd.split(' '), { ...opts }) }