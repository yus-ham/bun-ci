<?php
// usage token=Secret123&project_dir=project_name
$CI_SECRET = 'xxx';
$BASE_DIR = '/var/www/html/';


$_SERVER['REQUEST_METHOD'] === 'POST' OR die(header('HTTP/1.1 405').'#METHOD_NOT_ALLOWED');

$token = $_SERVER['HTTP_X_GITLAB_TOKEN'] ??null;
$token OR die(header('HTTP/1.1 400').'#NO_TOKEN');

$opts = [];
parse_str($token, $opts);
$opts['token'] === $CI_SECRET OR die(header('HTTP/1.1 400').'#INVALID_TOKEN');

header('Content-Type: text/plain');
$data = json_decode(file_get_contents('php://input'));
$project = preg_replace('/\'"|\.\./', '', ($opts['project_dir'] ??null) ?: explode('/',$data->project->path_with_namespace)[1]);

chdir($BASE_DIR.$project);
$branch = explode('/', $data->ref)[2];
$host_branch = trim(explode('/', file_get_contents('.git/HEAD'))[2]);
$remote = parse_ini_file('.git/config', $section=true)["branch $host_branch"]["remote"];

if ($host_branch != $branch) {
    echo 'Branch not matched, Skip!';
} else {
    foreach ([
        "git fetch $remote $branch",
        "git merge $remote/$branch --no-edit",
        "composer --no-interaction --prefer-dist -o i",
        "php yii migrate --interactive=0",
    ] as $cmd) {
        $result = null;
        print "\n$cmd\n";
        passthru($cmd, $result);
        $result and print "Exit code: $result\n";
    }
}

echo "\n\ngit status\n";
system("git status");
echo "git log\n";
exec("git log -n 5", $output);

$commit = null;
foreach ((array)$output as $line) {
  if (trim($line)) {
    if (strpos($line, 'commit ') === 0) {
      $commit and print_r($commit);
      $commit = [$line];
    }
    else $commit[] = $line;
  }
}
$commit and print_r($commit);